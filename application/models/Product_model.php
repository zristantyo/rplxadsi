<?php
class Product_model extends CI_Model{
    public $id_product;
    public $nama_product;
    public $tersedia;
    public $foto_url;
    public $harga;
    public $create_at;
    public $update_at;

    public function getProduct()
    {
        $this->load->database();
        $products = $this->db->get("product");
        $result = $products->result();
        return json_encode ($result);
    }
}